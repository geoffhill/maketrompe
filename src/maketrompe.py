#!/usr/bin/env python2

# Copyright (c) 2011, Geoff Hill
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
#   1. Redistributions of source code must retain the above copyright notice,
#      this list of conditions and the following disclaimer.
#
#   2. Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#

import math
from cv import *
from argparse import ArgumentParser
from sys import exit

output_width = 2550
output_height = 3300
output_size = (output_width, output_height)

def is_parallelogram(polygon):
    area = math.fabs(ContourArea(polygon))
    is_convex = CheckContourConvexity(polygon)
    return (len(polygon) == 4) and (area > 50) and is_convex

def reorient(polygon):
    (a, b, c, d) = polygon
    return ((a, b, c, d), (b, c, d, a))

def make_canny(photo, sensitivity):
    size = GetSize(photo)

    yuv = CreateImage(size, 8, 3)
    gray = CreateImage(size, 8, 1)
    CvtColor(photo, yuv, CV_BGR2YCrCb)
    Split(yuv, gray, None, None, None)
    
    canny = CreateImage(size, 8, 1)
    Canny(gray, canny, sensitivity, int(sensitivity * 1.9), 3)
    Dilate(canny, canny)
    return canny

def make_contours(canny):
    sto = CreateMemStorage()
    return FindContours(canny, sto, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE)

def make_pages(contours):
    sto = CreateMemStorage()
    pages = []
    while contours:
        arclen = ArcLength(contours)
        polygon = ApproxPoly(contours, sto, CV_POLY_APPROX_DP, arclen * 0.05, 0)
        for p in polygon:
            print p,
        print
        if is_parallelogram(polygon):
            pages.append(polygon)
        contours = contours.h_next()
    return pages

def make_trompe(photo, graphic, out, sensitivity):
    canny = make_canny(photo, sensitivity)
    SaveImage("%s-canny.jpg" % (out,), canny)

    contours = make_contours(canny)
    pages = make_pages(contours)
        
    for n, polygon in enumerate(pages):
        (polyw, polyh) = reorient(polygon)
        imgw = make_image(graphic, polyw)
        imgh = make_image(graphic, polyh)
        SaveImage("%s-%dw.jpg" % (out, n), imgw)
        SaveImage("%s-%dh.jpg" % (out, n), imgh)
    
def make_image(graphic, polygon):
    img = CreateImage(output_size, 8, 3)

    trans_mat = CreateMat(3, 3, CV_32F)
    dst_pts = [(output_width, output_height),
               (0, output_height),
               (0, 0),
               (output_width, 0)]
    GetPerspectiveTransform(polygon, dst_pts, trans_mat)
    WarpPerspective(graphic, img, trans_mat)
    Flip(img)
    return img

def bad_file():
    print "Bad file input, please try a different filename."
    exit(-1)

def main():
    parser = ArgumentParser(description="Create a Trompe-l'oeil image.")
    parser.add_argument('photo', type=str, help="file path for input photo")
    parser.add_argument('graphic', type=str, help="file path for input graphic")
    parser.add_argument('out', type=str, help="file path prefix for output")
    parser.add_argument('--canny', type=int, metavar='S', default=210, help="canny sensitivity parameter")
    args = parser.parse_args()
    try:
        photo = LoadImageM(args.photo)
        graphic = LoadImageM(args.graphic)
    except IOError:
        bad_file()
    make_trompe(photo, graphic, args.out, args.canny)

if __name__ == "__main__":
    main()

