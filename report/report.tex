%%% The ``\documentclass'' command has one parameter, based on the kind of
%%% document you are preparing.
%%%
%%% [annual] - Technical paper accepted for presentation at the ACM SIGGRAPH 
%%%   or SIGGRAPH Asia annual conference.
%%% [sponsored] - Short or full-length technical paper accepted for 
%%%   presentation at an event sponsored by ACM SIGGRAPH
%%%   (but not the annual conference Technical Papers program).
%%% [abstract] - A one-page abstract of your accepted content
%%%   (Technical Sketches, Posters, Emerging Technologies, etc.). 
%%%   Content greater than one page in length should use the "[sponsored]"
%%%   parameter.
%%% [preprint] - A preprint version of your final content.
%%% [review] - A technical paper submitted for review. Includes line
%%%   numbers and anonymization of author and affiliation information.

\documentclass[annual]{acmsiggraph}

%%% If you are submitting your paper to one of our annual conferences - the 
%%% ACM SIGGRAPH conference held in North America, or the SIGGRAPH Asia 
%%% conference held in Southeast Asia - there are several commands you should 
%%% consider using in the preparation of your document.

%%% 1. ``\TOGonlineID''
%%% When you submit your paper for review, please use the ``\TOGonlineID''
%%% command to include the online ID value assigned to your paper by the
%%% submission management system. Replace '45678' with the value you were
%%% assigned.

%\TOGonlineid{45678}

%%% 2. ``\TOGvolume'' and ``\TOGnumber''
%%% If you are preparing a preprint of your accepted paper, and your paper
%%% will be published in an issue of the ACM ``Transactions on Graphics''
%%% journal, replace the ``0'' values in the commands below with the correct
%%% volume and number values for that issue - you'll get them before your
%%% final paper is due.

%\TOGvolume{0}
%\TOGnumber{0}

%%% 3. ``TOGarticleDOI''
%%% The ``TOGarticleDOI'' command accepts the DOI information provided to you
%%% during production, and which makes up the URLs which identifies the ACM
%%% article page and direct PDF link in the ACM Digital Library.
%%% Replace ``1111111.2222222'' with the values you are given.

\TOGarticleDOI{1111111.2222222}

%%% 4. ``\TOGprojectURL'', ``\TOGvideoURL'', ``\TOGdataURL'', ``\TOGcodeURL''
%%% If you would like to include links to personal repositories for auxiliary
%%% material related your research contribution, you may use one or more of
%%% these commands to define an appropriate URL. The ``\TOGlinkslist'' command
%%% found just before the first section of your document will add hyperlinked
%%% icons to your document, in addition to hyperlinked icons which point to
%%% the ACM Digital Library article page and the ACM Digital Library-held PDF.

%\TOGprojectURL{http://bitbucket.org/geoffhill/maketrompe}
%\TOGvideoURL{http://bitbucket.org/geoffhill/maketrompe}
%\TOGdataURL{http://bitbucket.org/geoffhill/maketrompe}
\TOGcodeURL{http://bitbucket.org/geoffhill/maketrompe}

%%% Replace ``PAPER TEMPLATE TITLE'' with the title of your paper or abstract.

\title{Automatic Trompe l'Oeil generation}

%%% The ``\author{}'' command takes the names and affiliations of each of the
%%% authors of your paper or abstract. The ``\thanks{}'' command takes the
%%% contact information for each author.
%%% For multiple authors, separate each author's information by the ``\and''
%%% command.

\author{Geoff Hill\thanks{e-mail: geoffhill@u.northwestern.edu}\\ Northwestern University}

%%% The ``pdfauthor'' command accepts the authors of the work,
%%% comma-delimited, and adds this information to the PDF metadata.

\pdfauthor{Geoff Hill}

%%% Keywords that describe your work. The ``\keywordlist'' command will print
%%% them out.

\keywords{Internet imagery, Photo manipulation}

%%% The ``\begin{document}'' command is the start of the document.

%%% If you have user-defined macros, you may include them here.

% example of a user-defined macro called ``remark.''
% \newcommand{\remark}[1]{\textcolor{red}{#1}}

\begin{document}

%%% A ``teaser'' image appears under the title and affiliation information,
%%% horizontally centered, and above the two columns of text. This is OPTIONAL.
%%% If you choose to have a ``teaser'' image, it needs to be placed between
%%% ``\begin{document}'' and ``\maketitle.''

\teaser{
   \includegraphics[height=1.5in]{images/graphic-photo.jpg}
   \includegraphics[height=1.5in]{images/graphic-canny.jpg}
   \includegraphics[height=1.5in]{images/graphic-result.jpg}
   \caption{Photos from the process of using the utility. (a) Shows the input photo to the {\em maketrompe} utility, which is used to determine the projection coordinates of the bounding box of each environmental page. (b) Shows the result of processing the input photo with the Canny edge detector. (c) Shows a photo taken after the process is complete and the environmental pages have been replaced by their respective output pages. Not shown here is the input graphic.}
}

%%% The ``\maketitle'' command must appear after ``\begin{document}'' and,
%%% if you have one, after the definition of your ``teaser'' image, and
%%% before the first ``\section'' command.

\maketitle

%%% Your paper's abstract goes in its own section.

\begin{abstract}

Many artists and engineers employ orthographic simulation imaging as a technique to either deceive viewers or catch their attention. These images are perfectly distinguishable from a single center of projection---at this point, the viewer sees the image as if the viewer were looking head-on into a flat surface, regardless of the underlying topography. A name commonly used to refer to these images is {\em Trompe l'Oeil}, which translates from French to English as ``{\em deceive the eye}.''

The current production methods are mostly ad-hoc, and can be tedious and costly. Projectors are often used to ``paint'' the topography so as to indicate to artists where the image should be drawn. This method requires extensive human effort to calibrate the projector, and limits the image to simple shapes and outlines.

This paper presents a utility, {\em maketrompe}, that moves the projection from hardware to software. The utility uses computer vision methods to analyze the target environment, and image transformation methods to reproject the graphic into the target environment. The process is streamlined so that the only tools required from start to finish are a camera, a commodity printer and open-source software.
\end{abstract}

%%% ACM Computing Review (CR) categories.
%%% See <http://www.acm.org/class/1998/> for details.
%%% The ``\CRcat'' command takes four arguments.

\begin{CRcatlist}
  \CRcat{I.4.0}{Image processing and computer vision}{General}{Image processing software};
\end{CRcatlist}

%%% The ``\keywordlist'' command prints out the keywords.

\keywordlist

%%% The ``\TOGlinkslist'' command will insert hyperlinked icon(s) to your
%%% paper. This includes, at a minimum, hyperlinked icons to the ACM article
%%% page and the ACM Digital Library-held PDF. If you added URLs to
%%% ``\TOGprojectURL'' or the other, similar commands, they will be added to
%%% the list of icons.
%%% Note: this functionality only works for annual-conference papers.

%\TOGlinkslist

%%% The ``\copyrightspace'' command 
%%% Do not remove this command.

\copyrightspace

%%% This is the first section of the body of your paper.

\section{Goals}

The goal requirements of the utility {\em maketrompe} are as follows:
\begin{enumerate}
\item Must be {\em easy to use} and {\em fully automatic}. Only images are required as input; requires no mandatory parameters for the majority of images.
\item Must produce an {\em accurate projection}. If the user replaces all environment pages with their corresponding graphics pages, the image should be accurately displayed when viewed from the camera's center of projection.
\item Must preserve {\em arbitrarily-high resolution graphics}. An image loses definition when it is perspectively skewed, so high resolution must be maintained.
\end{enumerate}

\section{Software Usage}

The code can be obtained from the following link:

{\small \tt
\href{http://bitbucket.org/geoffhill/maketrompe}{http://bitbucket.org/geoffhill/maketrompe}
}

The {\em maketrompe} utility has the following software dependencies, and has been tested and known to work with version number in parentheses:
\begin{itemize}
\item Python (2.7.2)
\item Python-OpenCV (2.3.1)
\end{itemize}

Running the script in Python with the {\tt -h} flag shows a help menu describing the usage of the utility.

{\small
\begin{verbatim}
[geoff@geoff-laptop src]$ ./maketrompe.py -h
usage: maketrompe.py [-h] [--canny S] photo graphic out

Create a Trompe-l'oeil image.

positional arguments:
  photo       file path for input photo
  graphic     file path for input graphic
  out         file path prefix for output

optional arguments:
  -h, --help  show this help message and exit
  --canny S   canny sensitivity parameter
\end{verbatim}
}

\section{Procedure}

To create a Trompe l'Oeil image, use the following procedure.
\begin{enumerate}
\item Print out pages with a solid black rectangle covering the entire printable area of the paper. These are called the {\em environmental pages}. These pages have a one-to-one correspondance with the output pages, so if you want an output image that spans 8 output pages, then print 8 environmental pages.
\item Attach these environmental pages to any wall, floor or ceiling in the exact positions of the desired output pages.
\item Take a photo of the environmental pages from the desired center of projection.
\item Apply post-processing to crop to bound the environmental pages and increase contrast (if neccessary).
\item Create an input graphic of the exact size of the input photo. Using a tool such as Photoshop or the GIMP, a user can easily overlay the graphic over the input photo to preview and align.
\item Run the utility to produce the output images. Since the utility cannot distinguish paper orientation, the utility will produce a portrait- and a landscape-oriented version of each output page. It is up to the user to distinguish which version is useful.
\item Print the output pages and replace each environmental page with its corresponding output page.
\end{enumerate}


\section{Examples}

Included in the source is a {\tt test} directory, containing several environmental photographs and corresponding graphics. Here are some example commands for which example graphics are provided. Note that the paths have been truncated to easily fit the command on a single line.

{\small
\begin{verbatim}
maketrompe kitchen/2.jpg kitchen/2-cage.jpg out
maketrompe kitchen/2.jpg kitchen/2-rubiks.jpg out
maketrompe kitchen/7.jpg kitchen/7-cage.jpg out
maketrompe kitchen/7.jpg kitchen/7-rubiks.jpg out
maketrompe bedroom/2.jpg bedroom/2-cage.jpg out
maketrompe bedroom/2.jpg bedroom/2-text.jpg out
maketrompe bedroom/3.jpg bedroom/3-cage.jpg --canny 120 out
\end{verbatim}
}

\section{Conclusion}

The utility currently works extremely well on photos taken in good lighting and high contrast, and when the environmental pages take up as large of a portion of the photo as possible. This can be accomplished after the input photo is taken using post-processing techniques such as contrast enhancement and image cropping.

Since the release version of the code comes in at less than 100 SLOC of Python code, it is easily comprehensible and modifiable with minimal programmer effort. Therefore, even if the results of using the program as-is may be less than satisfactory, the programmer can adjust program values to obtain the expected result. Adding more of these values as optional program parameters is a work-in-progress.

\end{document}
